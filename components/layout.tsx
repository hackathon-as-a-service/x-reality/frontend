import './layout.scss';

import { Fragment, memo, ReactNode } from 'react';
import Link from 'next/link';

type LayoutProps = {
    children: ReactNode;
};
export const Layout = memo<LayoutProps>(props => {
    return (
        <Fragment>
            <nav>
                <Link href={'/'}>
                    <div className={'nav__vendor'}>
                        <img src={'/static/images/logo.svg'} />
                        <span>{'XReality'}</span>
                        <span>{'Store'}</span>
                    </div>
                </Link>
            </nav>
            <div className={'main'}>
                <div className={'main-con'}>{props.children}</div>
            </div>
        </Fragment>
    );
});
