const proxy = require('express-http-proxy');
const app = require('express')();
const cors = require('cors');

app.use(
    cors({
        origin: 'https://localhost:3000',
        credentials: true
    })
);

app.use(proxy('https://api.barbra.io/'));

app.listen(7070);
