import cookies from 'js-cookie';
import { createModel } from '@rematch/core';
import { Dispatch, store } from '../store';
import { queryViewer } from '~/endpoint/viewer/viewerRepository';
import { accountMetadataProperties } from '~/endpoint/account/accountProperties';
import { GQLAccount } from '~/endpoint/schema';

export const viewer = createModel<GQLAccount | null>({
    state: null,
    reducers: {
        login(state, payload: { account: GQLAccount }) {
            return payload.account;
        },
        logout(state, payload: unknown) {
            return null;
        }
    },
    effects: dispatch => ({
        // handle state changes with impure functions.
        // use async/await for async actions
        async query(payload, rootState) {
            const viewerData = await queryViewer(
                accountMetadataProperties()
            ).then(response => {
                return response.data;
            });

            if (viewer !== null) {
                dispatch.viewer.login(viewerData);
            } else if (viewer === null) {
                cookies.remove('session');
                dispatch.viewer.logout();
            }
        }
    })
});
