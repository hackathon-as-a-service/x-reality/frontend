import Document, {
    Head,
    Main,
    NextDocumentContext,
    NextScript
} from 'next/document';

export default class DocumentPage extends Document {
    static async getInitialProps(context: NextDocumentContext) {
        const initialProps = await Document.getInitialProps(context);

        return { ...initialProps };
    }

    render() {
        return (
            <html lang={'en'}>
                <Head>
                    <meta
                        name="viewport"
                        content="width=device-width,initial-scale=1.0,maximum-scale=5"
                    />

                    <link
                        rel="icon"
                        type="image/png"
                        sizes="16x16"
                        href={'/static/images/favicon/favicon-16x16.png'}
                    />
                    <link
                        rel="icon"
                        type="image/png"
                        sizes="32x32"
                        href={'/static/images/favicon/favicon-32x32.png'}
                    />
                </Head>
                <body>
                    <Main />
                    <NextScript />
                </body>
            </html>
        );
    }
}
