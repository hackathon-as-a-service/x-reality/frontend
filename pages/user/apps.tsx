import './apps.scss';

import { memo, ReactNode } from 'react';
import { Layout } from '~/components/layout';
import { ImmerComponent } from '~/components/_base';
import { NextContext } from 'next';
import Link from 'next/link';
import Head from 'next/head';

type UserProps = {
    initalUser: {
        name: string;
        apps: {
            id: string;
            name: string;
            img: string;
            category: string;
            downloads: {
                vive: string | null;
                oculus: string | null;
                google: string | null;
                micorsoft: string | null;
            };
            isPublic: boolean;
        }[];
    };
};
export default class User extends ImmerComponent<UserProps> {
    static async getInitialProps(ctx: NextContext): Promise<UserProps> {
        return {
            initalUser: {
                name: 'Eric',
                apps: [
                    {
                        id: 'appId',
                        name: 'test',
                        img:
                            'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAilBMVEX////tHCTsAAD84OHtEBrxY2bsCBXtDhntFh/sABDtEx34tLbsAAv70tP0iYzze37+8/P829z3rK795eb5vsD96+zwVFj5x8jzd3rzgIP5wsTwUVX70dL1k5bvPUPwWFzxZWnwSE3uLTT1mZvzhIbvOj/uLTP4sbPvQkfyb3P+9vbuNDr2oKL0j5G/lkIGAAAHyElEQVR4nO2dbXuiOhCGYYq8+1q1aqtotbW22///9xatPUeSwAxICOw196fdvRbJA8nMZDIJlsUwDMMwDMMwDMMwDMMwDMMwDMMwDMMwDMMwDMOU5rE/newHh+FisTzsJ9OXx0/TLaqPh+i7Z0NKHLg/BPH5r35yOI47r7P/J/EBXMe3FYRuDO7z+sV0I+9hDoGj0nYrMwDoHWemW1qVHqbvByeGJDLd1moQFZ5FAgzHpptbAbrCFBd2U9MNLk0phemYhNej6SaXpKRC2/bhvVvvsbTCs8aP9rqPz61oKyooPPfV08hI+1EiFx6Ef6qkMLU5wcSIgmJGCdh1KbRteG5dDLCKPbtGhal/bJnFWcLlydenMP2xhRElaka72K5doR28taanjt2rkHoV2mHcEr8xhd+5Uc0KU9/YihDnD/zXoroVpr+4NqIpwwBu2lO7QhuWRlTdsITb5tSv0LhJzQjUotCwxAFkG6NDodGO+geEtmhRaNDcTEFsih6Ftimn0RcFalNogxHXP3KlJKg2hX5sIoD7kFuvTaEdvjUvcBnL7dCn0A4a9xkraRDqVWg3PV8cKd6gXoU2NDsUE69xhc5zkwIjVR/VrNCGBtNTn25OG7QqtIPmkoyLKgp972dh9LJU6ikXFBHcU1MCx+o+WqDQdwHeT+voqT9+GPefovXpFUCOGDCg35DCXVhOoQf+ciVawtlq6UNOX8gj3DUjUOkK8xUGcMqLKl8WcVBKYkNO8Su3d8kKvQD2RfZhtIcyGv1XncJ+yfEUSoUJfKO/twalb825QxOL4fmvUFb4/Uj4wVkv/5lJL/FLh6Qs+aNQoZBIBHm2S77Fql45Cp4LGlNVoTXbUEdjqD12y/WFdym0rC21p4Luio1lkQu7Q6F1IEp0h/WJUVLYjnsUWt9EiVCbFiVSeq0+hWLyNfcmeh1GUjhZuE+htSWZGyepSYuSUfFjvlOhtSE5Da2T/YJ4hqBwPFkmu80uWU5y7OFMmRuR7qKzm26LZ7RFCtMoG2LX8X3fcdM/LZTRePEwv+L0dMmzEEtapHC1EWZKLmxU0UmPEqNqtKZyHp+mcPYMcjDrq2pmZpSXqDHHv0eMXY7CI6g7t6NYcVkT7Gmw16Zwjtg6tcICPwcD6X8TXmI416YQy5wpFS4KI1kpWb8n2FNPl8AH7PmqFCKRivQW1dl04SJd0XeE3VyhEPGgCu+Wl6q8IdblEQfYvWWFBNsohigv+CWuPHzroTgoVSqc40lvR7QbNppHDXWFpuitJYVPJPf2lL2ocAp6wbf1CPxEmysp/KCE0uFH9qKiTNDvjfRsl3osrRCLgX4vyybrKWOXksMrD95eUeGQlrUXExOv6EDUtICBB/6iQmoCLchedkLDb03Z/QnqiwWFhXm5zHVZD47HprGexVIs7pYU4o9E3WA0stAVe6MOX1R4oC6euYfMdbiP0eTy8QYLCslr3MKsHTdpwiOpC9wyCuMpdyVVRFj5xMevprQwHhIL73BDXcb2N5nr0DmM7eqpkPr33yFhHGYdcefGIcGWZmPoztlS3B8KM9PO+UO8wUG2ILtzMQ0el3pC0VLX4lLcAPjv2Su6NrfA54dizqVr80N8ji9VSnRsjk9IEbnC9paO5WmsBH0jvi9egq8leWKuzTeXa8NdvrQs1LF8Ke6J5ZC4WzlvigePxUpEpIhEKu0zum5hESyjHE8NC9eepEkCHhumkbougej64aXN0lUFb1FRnGl2/ZDyfAN5s2CUuwYsjyfDa8AEM6csd5nNFeWVIczbt45P6UK2p3JWTx9SLcbHk+L/Efyn3so2pJ7m2gClLe8Pg3M9TeiH53qaYKiMnVHfckZrPQ2pBbllWePJobfb7HqH3Joo2s/rrIlC6tquVN4O+Uara9O6OQhdBr4QbCv9eBtqE4nd1IYquTBilbDuDQnUQt7ysTGxgFZ3jTBh8nZtR9m3SK7z1n3CAj1/ti31u+2p1S/cb5GhzCFIs7f27LegZFF+G0O2Ce3aM1O070lqTkJ5jbOkXfueqA7jgkfYu/bdur1rhITtDQGsO7f/kFZufqsxfw/pqZS+5g5WIK98XvHAGU6lfcDToVOmf55pah8wNVl/y3kv93Z9vO7lPq6371B2m7Pd4F5u61S+cekYcm7246sP+EZobj++NSo3euqiwTMVrGP5fno/zZ4WNS9pI2pAWt/QC20PVq00fVRUSad4P82fSTts9i3GBs4W3NR69gyCs8EbVDuzoIpPq4YfGDlwt0JoUxUzZ+6Vm0fdJ9DYxy/2zUgEfWtNKNQM4H0C9RQiEilc4K1JoO5jIkxLNC1Qe0c120V/0GpuTBqZ/4kUW9HroSVnsqeuH/1sVTWcoCXn6p8PQdIRhseb1nwbwdJiUk0fxi4yjeud9Xtxy75RcqmZqVGgqtbGPMegSpJRhRu0xIaKjE70VbIC2vu9p5T+7m7f6MOuscx2Jaavd2n04bV1FkYi+qrcV0P46saHHlfPFVZdzqs3zw0sYdfEeAk5NaV5dPAbllFCF5nK6+R3SGdRDyDAxuTlW7JRG/07jZf93IHYDVX21T9/D9ibd/t7wBfG0SBRf9N5EHVs6BXx+fNd7uVwOFwO/rXvcjMMwzAMwzAMwzAMwzAMwzAMwzAMwzAMwzAMwzTGXxaidiHpPPEhAAAAAElFTkSuQmCC',
                        category: 'Marketing',
                        downloads: {
                            google: 'https://google.de',
                            micorsoft: 'null',
                            oculus: 'null',
                            vive: 'null'
                        },
                        isPublic: true
                    }
                ]
            }
        };
    }

    render() {
        return (
            <Layout>
                <Head>
                    <title>
                        {this.props.initalUser.name +
                            ' - Apps | XReality Store'}
                    </title>
                </Head>
                <div className={'user'}>
                    <div className={'user-side'}>
                        <li className={'--active'}>
                            <svg viewBox="0 0 57 57">
                                <path
                                    d="M22.66,0H3.34C1.498,0,0,1.498,0,3.34v19.32C0,24.502,1.498,26,3.34,26h19.32c1.842,0,3.34-1.498,3.34-3.34V3.34
		C26,1.498,24.502,0,22.66,0z"
                                />
                                <path
                                    d="M33.34,26h19.32c1.842,0,3.34-1.498,3.34-3.34V3.34C56,1.498,54.502,0,52.66,0H33.34C31.498,0,30,1.498,30,3.34v19.32
		C30,24.502,31.498,26,33.34,26z"
                                />
                                <path
                                    d="M22.66,30H3.34C1.498,30,0,31.498,0,33.34v19.32C0,54.502,1.498,56,3.34,56h19.32c1.842,0,3.34-1.498,3.34-3.34V33.34
		C26,31.498,24.502,30,22.66,30z"
                                />
                                <path
                                    d="M55,41H45V31c0-1.104-0.896-2-2-2s-2,0.896-2,2v10H31c-1.104,0-2,0.896-2,2s0.896,2,2,2h10v10c0,1.104,0.896,2,2,2
		s2-0.896,2-2V45h10c1.104,0,2-0.896,2-2S56.104,41,55,41z"
                                />
                            </svg>
                            <span>{'Apps'}</span>
                        </li>
                        <Link href={'/user/settings'}>
                            <li>
                                <svg viewBox="0 0 16 16" fill="none">
                                    <path
                                        fillRule="evenodd"
                                        clipRule="evenodd"
                                        d="M13.5725 8.735C13.6025 8.495 13.625 8.255 13.625 8C13.625 7.745 13.6025 7.505 13.5725 7.265L15.155 6.0275C15.2975 5.915 15.335 5.7125 15.245 5.5475L13.745 2.9525C13.655 2.7875 13.4525 2.7275 13.2875 2.7875L11.42 3.5375C11.03 3.2375 10.61 2.99 10.1525 2.8025L9.8675 0.815C9.845 0.635 9.6875 0.5 9.5 0.5H6.5C6.3125 0.5 6.155 0.635 6.1325 0.815L5.8475 2.8025C5.39 2.99 4.97 3.245 4.58 3.5375L2.7125 2.7875C2.54 2.72 2.345 2.7875 2.255 2.9525L0.755002 5.5475C0.657502 5.7125 0.702502 5.915 0.845002 6.0275L2.4275 7.265C2.3975 7.505 2.375 7.7525 2.375 8C2.375 8.2475 2.3975 8.495 2.4275 8.735L0.845002 9.9725C0.702502 10.085 0.665002 10.2875 0.755002 10.4525L2.255 13.0475C2.345 13.2125 2.5475 13.2725 2.7125 13.2125L4.58 12.4625C4.97 12.7625 5.39 13.01 5.8475 13.1975L6.1325 15.185C6.155 15.365 6.3125 15.5 6.5 15.5H9.5C9.6875 15.5 9.845 15.365 9.8675 15.185L10.1525 13.1975C10.61 13.01 11.03 12.755 11.42 12.4625L13.2875 13.2125C13.46 13.28 13.655 13.2125 13.745 13.0475L15.245 10.4525C15.335 10.2875 15.2975 10.085 15.155 9.9725L13.5725 8.735ZM8 10.625C6.5525 10.625 5.375 9.4475 5.375 8C5.375 6.5525 6.5525 5.375 8 5.375C9.4475 5.375 10.625 6.5525 10.625 8C10.625 9.4475 9.4475 10.625 8 10.625Z"
                                        fill="#444444"
                                    />
                                </svg>
                                <span>{'Settings'}</span>
                            </li>
                        </Link>
                    </div>
                    <div className={'user-content'}>
                        <div className={'user-content__manager'}>
                            <div className={'user-content__manager__hero'}>
                                {'Your Apps'}
                            </div>
                            <Link href={'/app/publish'}>
                                <button>{'Publish new App'}</button>
                            </Link>
                        </div>

                        <div className={'user-content__apps'}>
                            <div className={'user-content__apps__names'}>
                                <li>{'Name'}</li>
                                {this.props.initalUser.apps.map(app => {
                                    return (
                                        <Link
                                            key={app.name}
                                            href={'/app?id=' + app.id}
                                        >
                                            <li>
                                                <img src={app.img} />
                                                <span>{app.name}</span>
                                            </li>
                                        </Link>
                                    );
                                })}
                            </div>

                            <div className={'user-content__apps__categories'}>
                                <li>{'Category'}</li>
                                {this.props.initalUser.apps.map(app => {
                                    return (
                                        <Link
                                            key={app.name}
                                            href={'/app?id=' + app.id}
                                        >
                                            <li>{app.category}</li>
                                        </Link>
                                    );
                                })}
                            </div>

                            <div className={'user-content__apps__devices'}>
                                <li>{'Devices'}</li>
                                {this.props.initalUser.apps.map(app => {
                                    return (
                                        <Link
                                            key={app.name}
                                            href={'/app?id=' + app.id}
                                        >
                                            <li>
                                                {Object.keys(app.downloads)
                                                    .map(
                                                        key =>
                                                            key[0].toUpperCase() +
                                                            key.slice(
                                                                1,
                                                                key.length
                                                            )
                                                    )
                                                    .join(', ')}
                                            </li>
                                        </Link>
                                    );
                                })}
                            </div>
                            <div className={'user-content__apps__permissions'}>
                                <li>{'Permission'}</li>
                                {this.props.initalUser.apps.map(app => {
                                    return (
                                        <Link
                                            key={app.name}
                                            href={'/app?id=' + app.id}
                                        >
                                            <li>
                                                {app.isPublic
                                                    ? 'Public'
                                                    : 'Private'}
                                            </li>
                                        </Link>
                                    );
                                })}
                            </div>
                        </div>
                    </div>
                </div>
            </Layout>
        );
    }
}
