import './index.scss';

import { ImmerComponent } from '~/components/_base';
import { NextContext } from 'next';
import { Layout } from '~/components/layout';
import Head from 'next/head';

const oculusLogo = require('~/images/vendors/oculus.png');
const msLogo = require('~/images/vendors/ms.png');
const viveLogo = require('~/images/vendors/vive.png');
const googleLogo = require('~/images/vendors/google.png');
const arrowSvg = require('~/images/arrow.svg');

type NewProps = {
    initalNew: {};
};
export default class New extends ImmerComponent<NewProps> {
    static async getInitialProps(ctx: NextContext): Promise<NewProps> {
        return {
            initalNew: {}
        };
    }

    render() {
        return (
            <Layout>
                <Head>
                    <title>{'Create app | XReality Store'}</title>
                </Head>
                <div className={'appEdit'}>
                    <div className={'appEdit__hero'}>
                        {'Describe your new app'}
                    </div>

                    <div className={'appEdit__data'}>
                        <div className={'appEdit__data__title'}>
                            {'What is the name of you app?'}
                        </div>
                        <div className={'appEdit__data__label'}>{'Name'}</div>
                        <input className={'appEdit__data__input'} />
                    </div>

                    <div className={'appEdit__data'}>
                        <div className={'appEdit__data__title'}>
                            {'Upload your app files for diffrent plattforms'}
                        </div>
                        <div className={'appEdit-upload'}>
                            <div>
                                <img src={msLogo} />
                                <label htmlFor={'upload'}>
                                    {'Upload for HoloLens'}
                                </label>
                                <input
                                    id={'upload'}
                                    name={'upload'}
                                    type="file"
                                    onChange={event => {
                                        const formData = new FormData();

                                        formData.append(
                                            'file',
                                            event.target.files![0]
                                        );

                                        fetch('/upload', {
                                            method: 'POST',
                                            body: formData
                                        });
                                    }}
                                />
                            </div>
                            <div>
                                <img src={viveLogo} />
                                <label htmlFor={'upload'}>
                                    {'Upload for Vive'}
                                </label>
                                <input
                                    id={'upload'}
                                    name={'upload'}
                                    type="file"
                                    onChange={event => {
                                        const formData = new FormData();

                                        formData.append(
                                            'file',
                                            event.target.files![0]
                                        );

                                        fetch('/upload', {
                                            method: 'POST',
                                            body: formData
                                        });
                                    }}
                                />
                            </div>
                            <div>
                                <img src={oculusLogo} />
                                <label htmlFor={'upload'}>
                                    {'Upload for Oculus'}
                                </label>
                                <input
                                    id={'upload'}
                                    name={'upload'}
                                    type="file"
                                    onChange={event => {
                                        const formData = new FormData();

                                        formData.append(
                                            'file',
                                            event.target.files![0]
                                        );

                                        fetch('/upload', {
                                            method: 'POST',
                                            body: formData
                                        });
                                    }}
                                />
                            </div>
                            <div>
                                <img src={googleLogo} />
                                <label htmlFor={'upload'}>
                                    {'Upload for GoogleCardboard'}
                                </label>
                                <input
                                    id={'upload'}
                                    name={'upload'}
                                    type="file"
                                    onChange={event => {
                                        const formData = new FormData();

                                        formData.append(
                                            'file',
                                            event.target.files![0]
                                        );

                                        fetch('/upload', {
                                            method: 'POST',
                                            body: formData
                                        });
                                    }}
                                />
                            </div>
                        </div>
                    </div>
                    <div className={'appEdit__data'}>
                        <div className={'appEdit__data__title'}>
                            {'Add images and a video preview to your app'}
                        </div>

                        <div className={'appEdit__data__label'}>
                            {'Preview Video'}
                        </div>
                        <input className={'appEdit__data__input'} />

                        <div className={'appEdit__data__label'}>
                            {'First Image'}
                        </div>
                        <input className={'appEdit__data__input'} />

                        <div className={'appEdit__data__label'}>
                            {'Second Image'}
                        </div>
                        <input className={'appEdit__data__input'} />

                        <div className={'appEdit__data__label'}>
                            {'Third Image'}
                        </div>
                        <input className={'appEdit__data__input'} />
                    </div>

                    <div className={'appEdit__data'}>
                        <div className={'appEdit__data__title'}>
                            {'Add a description to your app'}
                        </div>

                        <div className={'appEdit__data__label'}>
                            {'Description'}
                        </div>
                        <textarea className={'appEdit__data__input'} />
                    </div>

                    <div className={'appEdit__data'}>
                        <div className={'appEdit__data__title'}>
                            {'Do you wish to inivite anyone else to your app?'}
                        </div>

                        <div className={'appEdit__data__label'}>
                            {'Invite by Email'}
                        </div>
                        <input className={'appEdit__data__input'} />
                    </div>

                    <div className={'appEdit__save --deactivate'}>
                        <button>{'Save and Publish App'}</button>
                    </div>
                </div>
            </Layout>
        );
    }
}
