FROM node:10.13.0-slim

WORKDIR /server

COPY . .

RUN npm i
RUN npm run build

EXPOSE 4040

CMD ["npm", "start"]
