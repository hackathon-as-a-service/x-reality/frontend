#!/bin/bash

openssl genrsa -des3 -passout pass:12345 -out server.pass.key 2048
openssl rsa -passin pass:12345 -in server.pass.key -out server.key
openssl req -new -key server.key -out server.csr
openssl x509 -req -days 365 -in server.csr -signkey server.key -out server.crt