const spdy = require('spdy');
const path = require('path');
const fs = require('fs');
const express = require('express');
const helmet = require('helmet');
const fileUpload = require('express-fileupload');
const morgan = require('morgan');
const compression = require('compression');
const next = require('next');
const mime = require('mime-types');

const devMode = process.env.NODE_ENV === 'development';

const nextApp = next({ dev: devMode });
const handle = nextApp.getRequestHandler();

nextApp
    .prepare()
    .then(() => {
        const expressApp = express();

        // Add compression
        expressApp.use(compression());

        // Add logging
        expressApp.use(morgan(devMode ? 'dev' : 'common'));

        // Add security
        expressApp.use(helmet());

        // Add fileUpload
        expressApp.use(fileUpload());

        expressApp.post('/upload', (req, res) => {
            const file = req.files.file;
            const id = Math.random()
                .toString(36)
                .substring(3);
            const ext = mime.extension(file.mimetype);

            // Use the mv() method to place the file somewhere on your server
            file.mv(`./files/${id}.${ext}`, err => {
                if (err) {
                    return res.status(500).send(err);
                }

                res.send(
                    JSON.stringify({
                        url: `/file/${id}.${ext}`
                    })
                );
            });
        });

        expressApp.get('/file/:name', (req, res) => {
            res.sendFile(
                path.join(process.cwd(), './files/' + req.params.name)
            );
        });

        expressApp.use(handle);

        const spdyServer = spdy.createServer(
            {
                ...(devMode
                    ? {
                          key: fs.readFileSync(__dirname + '/ssl/server.key'),
                          cert: fs.readFileSync(__dirname + '/ssl/server.crt')
                      }
                    : {}),

                spdy: {
                    protocols: ['h2', 'http/1.1']
                }
            },
            expressApp
        );

        spdyServer.listen(4040);
    })
    .catch(ex => {
        console.error(ex.stack);
        process.exit(1);
    });
