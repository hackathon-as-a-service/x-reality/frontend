import { graphqlEndpoint } from '~/enviroment/urls';
import fetch from 'isomorphic-unfetch';

export const postQueryToEndpoint = <TResponseData = unknown>(
    query: string,
    cookies: string = ''
) => {
    return fetch(graphqlEndpoint, {
        method: 'POST',
        credentials: 'include',
        body: JSON.stringify({ query: query }),
        headers: {
            cookie: cookies
        }
    }).then(
        res =>
            res.json() as Promise<{
                data: TResponseData | null;
                errors: string[];
            }>
    );
};
